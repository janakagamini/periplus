package us.peripl.app.tasks;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import us.peripl.app.Constants;
import us.peripl.app.models.Address;
import us.peripl.app.models.Exhibit;
import us.peripl.app.models.Organisation;

public class ServicePinParseOrganisation extends IntentService {

    public static final String ACTION_PIN_SERVICE = "action_pin_service";
    public static final String PIN_SERVICE_MAX_PROGRESS = "pin_service_max_progress";
    public static final String PIN_SERVICE_CMD = "pin_service_cmd";
    public static final String PIN_SERVICE_RECEIVER_CMD = "pin_service_receiver";
    public static final String PIN_SERVICE_RECEIVER_INT_EXTRA_POSITION = "pin_service_receiver_int_extra_position";
    public static final String PIN_SERVICE_RECEIVER_INT_EXTRA_PROGRESS = "pin_service_receiver_int_extra_progress";
    public static final String PIN_SERVICE_RECEIVER_STRING_EXTRA_OBJECT_ID = "pin_service_receiver_string_extra_object_id";
    public static final String PIN_SERVICE_RECEIVER_STRING_EXTRA_ERROR = "pin_service_receiver_string_extra_error";
    public static final int ON_START = 0;
    public static final int ON_PROGRESS_UPDATE = 1;
    public static final int ON_END = 2;
    public static final int ON_ERROR = 3;
    private static final String TAG = ServicePinParseOrganisation.class.getCanonicalName();
    private boolean pin;
    private String objectId;
    private int currentProgress;
    private int maxProgress;
    private int position;

    public ServicePinParseOrganisation() {
        super("ServicePinParseOrganisation");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        pin = intent.getBooleanExtra(PIN_SERVICE_CMD, false);
        objectId = intent.getStringExtra(Constants.OBJECT_ID);
        position = intent.getIntExtra(PIN_SERVICE_RECEIVER_INT_EXTRA_POSITION, -1);
        maxProgress = intent.getIntExtra(PIN_SERVICE_MAX_PROGRESS, -1);

        if (position < 0 || maxProgress < 0) {
            broadcastError("position or max progress wrong");
            return;
        } else {
            broadcastStart();
        }

        Organisation organisation = ParseObject.createWithoutData(Organisation.class, objectId);

        if (pin) {
            currentProgress = 0;

            //Pin address
            try {
                pinAddress(organisation);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.d(TAG, "Pinning address failed, nothing to cleanup.");
                broadcastError("Could not pin address");
                return;
            }

            //Pin exhibits
            try {
                int exhibitCount = pinExhibits(organisation);
                Log.d(TAG, "Pinned " + exhibitCount + " exhibits");

            } catch (ParseException e) {
                e.printStackTrace();
                Log.d(TAG, "Pinning one or more exhibits failed, cleaning up...");
                broadcastError("Could not pin one or more exhibits.");

                //Cleanup any exhibits that might have been pinned
                try {
                    int exhibitCount = unpinExhibits(organisation, true);
                    Log.d(TAG, "Cleaned up " + exhibitCount + " exhibits");
                } catch (ParseException e1) {
                    e1.printStackTrace();
                    broadcastError("Could not unpin one or more exhibits, while cleaning up");
                }

                //Cleanup address previously pinned
                try {
                    unPinAddress(organisation, true);
                    Log.d(TAG, "Cleaned up address");
                } catch (ParseException e1) {
                    e1.printStackTrace();
                    broadcastError("Could not unpin address, while cleaning up");
                }

                return;
            }

            //Pin Organisation
            try {
                pinOrganisation(objectId);
                broadcastProgress(100);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.d(TAG, "Pinning organisation failed, cleaning up...");
                broadcastError("Could not pin organisation.");

                //Cleanup any exhibits that might have been pinned
                try {
                    int exhibitCount = unpinExhibits(organisation, true);
                    Log.d(TAG, "Unpinned " + exhibitCount + " exhibits");
                } catch (ParseException e1) {
                    e1.printStackTrace();
                    broadcastError("Could not unpin one or more exhibits, while cleaning up");
                }

                //Cleanup address previously pinned
                try {
                    unPinAddress(organisation, true);
                    Log.d(TAG, "Cleaned up address");
                } catch (ParseException e1) {
                    e1.printStackTrace();
                    broadcastError("Could not unpin address, while cleaning up");
                }

                return;
            }

        } else {
            currentProgress = maxProgress;

            try {
                unpinOrganisation(organisation, false);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.d(TAG, "Unpinning organisation failed...");
                broadcastError("Could not unpin organisation.");
                return;
            }

            try {
                unPinAddress(organisation, false);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.d(TAG, "Unpinning address failed...");
                broadcastError("Could not unpin address.");
                return;
            }

            try {
                unpinExhibits(organisation, false);
                broadcastProgress(0);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.d(TAG, "Unpinning exhibits failed...");
                broadcastError("Could not unpin exhibits.");
                return;
            }

        }

        broadcastEnd();

    }

    private Organisation pinOrganisation(String objectId) throws ParseException {
        ParseQuery<Organisation> query = new ParseQuery<Organisation>(Organisation.class);
        Organisation organisation = query.get(objectId);
        organisation.pin();
        broadcastProgress(getProgressPercentage(++currentProgress));
        return organisation;
    }

    private void unpinOrganisation(Organisation organisation, boolean isCleanup) throws ParseException {
        organisation.unpin();
        if (!isCleanup) {
            broadcastProgress(getProgressPercentage(--currentProgress));
        }
    }

    private void pinAddress(Organisation organisation) throws ParseException {
        ParseQuery<Address> query = new ParseQuery<Address>(Address.class);
        query.whereEqualTo(Address.ADDRESS_OF, organisation);
        Address address = query.getFirst();
        address.pin();
        broadcastProgress(getProgressPercentage(++currentProgress));
    }

    private void unPinAddress(Organisation organisation, boolean isCleanup) throws ParseException {
        ParseQuery<Address> query = new ParseQuery<Address>(Address.class);
        query.fromLocalDatastore();
        query.whereEqualTo(Address.ADDRESS_OF, organisation);
        Address address = query.getFirst();
        address.unpin();
        if (!isCleanup) {
            broadcastProgress(getProgressPercentage(--currentProgress));
        }
    }

    private int pinExhibits(Organisation organisation) throws ParseException {

        List<Exhibit> exhibits = new ArrayList<Exhibit>();
        int itemsPerPage = 100;
        int pageCount = 0;

        ParseQuery<Exhibit> query = new ParseQuery<Exhibit>(Exhibit.class);
        query.whereEqualTo(Exhibit.EXHIBIT_AT, organisation);
        query.orderByAscending(Exhibit.REF);
        query.setLimit(itemsPerPage);

        while (true) {

            query.setSkip(pageCount * itemsPerPage);
            List<Exhibit> temp = query.find();

            if (temp.size() > 0) {
                exhibits.addAll(temp);

                currentProgress = currentProgress + temp.size();
                broadcastProgress(getProgressPercentage(currentProgress));
            } else break;

            pageCount++;
        }

        for (Exhibit exhibit : exhibits) {
            exhibit.pin();
            broadcastProgress(getProgressPercentage(++currentProgress));
        }

        return exhibits.size();
    }

    private int unpinExhibits(Organisation organisation, boolean isCleanup) throws ParseException {

        List<Exhibit> exhibits = new ArrayList<Exhibit>();
        int itemsPerPage = 100;
        int pageCount = 0;

        ParseQuery<Exhibit> query = new ParseQuery<Exhibit>(Exhibit.class);
        query.fromLocalDatastore();
        query.whereEqualTo(Exhibit.EXHIBIT_AT, organisation);
        query.orderByAscending(Exhibit.REF);
        query.setLimit(itemsPerPage);

        while (true) {

            query.setSkip(pageCount * itemsPerPage);
            List<Exhibit> temp = query.find();

            if (temp.size() > 0) {
                exhibits.addAll(temp);

                if (!isCleanup) {
                    currentProgress = currentProgress - temp.size();
                    broadcastProgress(getProgressPercentage(currentProgress));
                }
            } else break;

            pageCount++;
        }

        for (Exhibit exhibit : exhibits) {
            exhibit.unpin();
            if (!isCleanup) broadcastProgress(getProgressPercentage(--currentProgress));
        }

        return exhibits.size();
    }

    private void broadcastStart() {
        Intent broadcastIntent = new Intent(ACTION_PIN_SERVICE);
        broadcastIntent.putExtra(PIN_SERVICE_CMD, pin);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_CMD, ON_START);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_INT_EXTRA_POSITION, position);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_STRING_EXTRA_OBJECT_ID, objectId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    private void broadcastProgress(int progress) {
        Intent broadcastIntent = new Intent(ACTION_PIN_SERVICE);
        broadcastIntent.putExtra(PIN_SERVICE_CMD, pin);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_CMD, ON_PROGRESS_UPDATE);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_INT_EXTRA_POSITION, position);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_INT_EXTRA_PROGRESS, progress);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_STRING_EXTRA_OBJECT_ID, objectId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    private void broadcastEnd() {
        Intent broadcastIntent = new Intent(ACTION_PIN_SERVICE);
        broadcastIntent.putExtra(PIN_SERVICE_CMD, pin);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_CMD, ON_END);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_INT_EXTRA_POSITION, position);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_STRING_EXTRA_OBJECT_ID, objectId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    private void broadcastError(String message) {
        Intent broadcastIntent = new Intent(ACTION_PIN_SERVICE);
        broadcastIntent.putExtra(PIN_SERVICE_CMD, pin);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_CMD, ON_ERROR);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_INT_EXTRA_POSITION, position);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_STRING_EXTRA_ERROR, message);
        broadcastIntent.putExtra(PIN_SERVICE_RECEIVER_STRING_EXTRA_OBJECT_ID, objectId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    private int getProgressPercentage(int i) {
        double progress = ((double) i / (double) maxProgress) * 100;

        return (int) progress;
    }
}
