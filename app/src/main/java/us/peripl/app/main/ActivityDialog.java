package us.peripl.app.main;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;

import us.peripl.app.Constants;
import us.peripl.app.R;
import us.peripl.app.util.ParseLocalObject;

public class ActivityDialog extends Activity {

    public static final String DIALOG_TYPE = "dialog_type";
    private static final String TAG = ActivityDialog.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dialog);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        DialogType dialogType = (DialogType) getIntent().getSerializableExtra(DIALOG_TYPE);
        boolean isPinnedOnly = getIntent().getBooleanExtra(Constants.IS_PINNED_ONLY, false);
        ParseLocalObject organisation = (ParseLocalObject) getIntent().getSerializableExtra(ParseLocalObject.PARSE_LOCAL_OBJECT);

        if (savedInstanceState == null) {

            switch (dialogType) {
                case ADDRESS:
                    getFragmentManager().beginTransaction()
                            .add(R.id.container, FragmentAddress.newInstance(organisation, isPinnedOnly))
                            .commit();
                    break;
                case ADMISSION:
                    getFragmentManager().beginTransaction()
                            .add(R.id.container, FragmentAdmission.newInstance(organisation, isPinnedOnly))
                            .commit();
                    break;
                case INFO:
                    getFragmentManager().beginTransaction()
                            .add(R.id.container, FragmentInfo.newInstance(organisation, isPinnedOnly))
                            .commit();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_dialog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static enum DialogType {
        ADDRESS,
        ADMISSION,
        INFO
    }
}
