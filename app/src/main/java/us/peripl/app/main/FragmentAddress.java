package us.peripl.app.main;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import us.peripl.app.Constants;
import us.peripl.app.R;
import us.peripl.app.models.Address;
import us.peripl.app.models.Organisation;
import us.peripl.app.util.GoogleStaticMap;
import us.peripl.app.util.ParseLocalObject;

public class FragmentAddress extends Fragment {

    private final static String TAG = FragmentAddress.class.getCanonicalName();

    private boolean isPinnedOnly;
    private ParseLocalObject organisation;

    public FragmentAddress() {
        // Required empty public constructor
    }

    public static FragmentAddress newInstance(ParseLocalObject organisation, boolean isPinnedOnly) {
        FragmentAddress fragment = new FragmentAddress();
        Bundle args = new Bundle();
        args.putSerializable(ParseLocalObject.PARSE_LOCAL_OBJECT, organisation);
        args.putBoolean(Constants.IS_PINNED_ONLY, isPinnedOnly);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            organisation = (ParseLocalObject) getArguments().getSerializable(ParseLocalObject.PARSE_LOCAL_OBJECT);
            isPinnedOnly = getArguments().getBoolean(Constants.IS_PINNED_ONLY, false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address, container, false);

        TextView name = (TextView) view.findViewById(R.id.name);
        ImageView staticMap = (ImageView) view.findViewById(R.id.static_map);
        final LinearLayout addressHolder = (LinearLayout) view.findViewById(R.id.addressHolder);
        final TextView streetAddress = (TextView) view.findViewById(R.id.streetAddress);
        final TextView addressLocality = (TextView) view.findViewById(R.id.addressLocality);
        final TextView addressRegion = (TextView) view.findViewById(R.id.addressRegion);
        final TextView postalCode = (TextView) view.findViewById(R.id.postalCode);
        final TextView addressCountry = (TextView) view.findViewById(R.id.addressCountry);

        name.setText((organisation.getString(Organisation.NAME)).toUpperCase());

        double[] latlong = organisation.getParseGeoPointArray(Organisation.GEO);

        int scale;

        switch (getActivity().getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                scale = 1;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                scale = 1;
                break;
            case DisplayMetrics.DENSITY_HIGH:
                scale = 2;
                break;
            default:
                scale = 2;
        }

        String mapUrl = GoogleStaticMap.getStaticMapUrl(15, 512, 288, scale, "red", latlong[0], latlong[1]);
        ImageLoader.getInstance().displayImage(mapUrl, staticMap);

        Organisation organisationShell = ParseObject.createWithoutData(Organisation.class, organisation.getObjectId());

        ParseQuery<Address> query = new ParseQuery<Address>(Address.class);
        if (isPinnedOnly) query.fromLocalDatastore();
        query.whereEqualTo(Address.ADDRESS_OF, organisationShell);
        query.getFirstInBackground(new GetCallback<Address>() {
            @Override
            public void done(Address address, ParseException e) {
                if (e == null) {
                    String _streetAddress = address.getStreetAddress();
                    String _addressLocality = address.getAddressLocality();
                    String _addressRegion = address.getAddressRegion();
                    String _postalCode = address.getPostalCode();
                    String _addressCountry = address.getAddressCountry();

                    streetAddress.setText(_streetAddress);

                    if (_addressLocality == null) addressLocality.setVisibility(View.GONE);
                    else addressLocality.setText(_addressLocality);

                    if (_addressRegion == null) addressRegion.setVisibility(View.GONE);
                    else addressRegion.setText(_addressRegion);

                    if (_postalCode == null) postalCode.setVisibility(View.GONE);
                    else postalCode.setText(_postalCode);

                    addressCountry.setText(_addressCountry);

                    addressHolder.setVisibility(View.VISIBLE);

                } else {
                    e.printStackTrace();
                    Log.d(TAG, "Querying address failed!");
                }
            }
        });


        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
