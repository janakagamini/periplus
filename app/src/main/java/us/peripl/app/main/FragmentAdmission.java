package us.peripl.app.main;


import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import us.peripl.app.Constants;
import us.peripl.app.R;
import us.peripl.app.models.AdmissionCharge;
import us.peripl.app.models.Organisation;
import us.peripl.app.util.ParseLocalObject;

public class FragmentAdmission extends Fragment {

    private final static String TAG = FragmentAdmission.class.getCanonicalName();

    private boolean isPinnedOnly;
    private ParseLocalObject organisation;

    public FragmentAdmission() {
        // Required empty public constructor
    }

    public static FragmentAdmission newInstance(ParseLocalObject organisation, boolean isPinnedOnly) {

        FragmentAdmission fragment = new FragmentAdmission();

        Bundle args = new Bundle();
        args.putBoolean(Constants.IS_PINNED_ONLY, isPinnedOnly);
        args.putSerializable(ParseLocalObject.PARSE_LOCAL_OBJECT, organisation);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            isPinnedOnly = getArguments().getBoolean(Constants.IS_PINNED_ONLY, false);
            organisation = (ParseLocalObject) getArguments().getSerializable(ParseLocalObject.PARSE_LOCAL_OBJECT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admission, container, false);

        TextView name = (TextView) view.findViewById(R.id.name);
        name.setText(organisation.getString(Organisation.NAME).toUpperCase());

        final TableLayout tableLayout = (TableLayout) view.findViewById(R.id.admissionChargesHolder);

        Organisation organisationShell = ParseObject.createWithoutData(Organisation.class, organisation.getObjectId());

        ParseQuery<AdmissionCharge> query = new ParseQuery<AdmissionCharge>(AdmissionCharge.class);
        if (isPinnedOnly) query.fromLocalDatastore();
        query.whereEqualTo(AdmissionCharge.CHARGE_FOR, organisationShell);
        query.findInBackground(new FindCallback<AdmissionCharge>() {
            @Override
            public void done(List<AdmissionCharge> admissionCharges, ParseException e) {
                if (e == null) {
                    for (AdmissionCharge admissionCharge : admissionCharges) {

                        View row = getActivity().getLayoutInflater().inflate(R.layout.include_admission_charge, null);

                        TextView description = (TextView) row.findViewById(R.id.description);
                        TextView priceCurrency = (TextView) row.findViewById(R.id.priceCurrency);
                        TextView price = (TextView) row.findViewById(R.id.price);

                        double priceValue = admissionCharge.getPrice();

                        description.setText(admissionCharge.getDescription());

                        if (priceValue != 0) {
                            priceCurrency.setText(admissionCharge.getPriceCurrency());
                            price.setText(String.valueOf(admissionCharge.getPrice()));
                        } else {
                            priceCurrency.setText("");
                            price.setTextColor(getResources().getColor(R.color.peri_bright));
                            price.setText("Free");
                        }

                        tableLayout.addView(row);

                    }
                } else {
                    e.printStackTrace();
                    Log.d(TAG, "Querying admission charges failed");
                }
            }
        });

        return view;
    }


}
