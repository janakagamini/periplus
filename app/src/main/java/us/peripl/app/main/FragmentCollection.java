package us.peripl.app.main;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.List;

import us.peripl.app.Constants;
import us.peripl.app.R;
import us.peripl.app.adapters.ParseCollectionAdapter;
import us.peripl.app.models.Exhibit;
import us.peripl.app.models.Organisation;

public class FragmentCollection extends Fragment {

    private static final String TAG = FragmentCollection.class.getCanonicalName();

    TextView emptyTextView;
    StaggeredGridView mGridView;
    ParseCollectionAdapter mAdapter;


    public FragmentCollection() {
    }

    public static FragmentCollection newInstance(int sectionNumber, String orgObjectId, String collectionSortKey, boolean isPinnedOnly) {

        FragmentCollection fragment = new FragmentCollection();

        Bundle args = new Bundle();
        args.putInt(Constants.ARG_SECTION_NUMBER, sectionNumber);
        args.putString(Constants.OBJECT_ID, orgObjectId);
        args.putString(Constants.PARSE_SORT_KEY, collectionSortKey);
        args.putBoolean(Constants.IS_PINNED_ONLY, isPinnedOnly);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((ActivityOrganisation) activity).onSectionAttached(getArguments().getInt(Constants.ARG_SECTION_NUMBER));

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setRetainInstance(true)
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_organisation_collection_list, container, false);

        emptyTextView = (TextView) view.findViewById(android.R.id.empty);
        mGridView = (StaggeredGridView) view.findViewById(R.id.grid);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        boolean isPinnedOnly = getArguments().getBoolean(Constants.IS_PINNED_ONLY);
        String objectId = getArguments().getString(Constants.OBJECT_ID);
        String collectionSortKey = getArguments().getString(Constants.PARSE_SORT_KEY);

        mAdapter = initAdapter(getActivity(), isPinnedOnly, objectId, collectionSortKey);
        mGridView.setAdapter(mAdapter);
        mGridView.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), true, true));

    }

    private ParseCollectionAdapter initAdapter(final Context context, final boolean isPinnedOnly, String objectId, final String collectionSortKey) {

        final Organisation organisationShell = ParseObject.createWithoutData(Organisation.class, objectId);

        ParseCollectionAdapter adapter = new ParseCollectionAdapter(context, new ParseQueryAdapter.QueryFactory<Exhibit>() {
            @Override
            public ParseQuery<Exhibit> create() {
                ParseQuery<Exhibit> query = new ParseQuery<Exhibit>(Exhibit.class);
                if (isPinnedOnly) query.fromLocalDatastore();
                query.whereEqualTo(Exhibit.EXHIBIT_AT, organisationShell);
                query.orderByAscending(collectionSortKey);

                return query;
            }
        });

        adapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<Exhibit>() {
            @Override
            public void onLoading() {
                ((Activity) context).setProgressBarIndeterminateVisibility(true);
                emptyTextView.setText("Periplus is updating this list...");
            }

            @Override
            public void onLoaded(List<Exhibit> exhibits, Exception e) {
                ((Activity) context).setProgressBarIndeterminateVisibility(false);
                if (e == null) {
                    Toast.makeText(context, "Completed updating list.", Toast.LENGTH_LONG).show();
                } else {
                    emptyTextView.setText("This list could not be updated.");
                }
            }
        });

        //TODO: Check the following statement setObjectsPerPage(25)
        adapter.setObjectsPerPage(25);
        adapter.setTextKey(Exhibit.NAME);

        return adapter;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();

        mAdapter.loadObjects();
    }

    @Override
    public void onPause() {
        ImageLoader.getInstance().stop();

        super.onPause();
    }

}