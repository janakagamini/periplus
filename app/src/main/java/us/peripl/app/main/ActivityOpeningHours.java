package us.peripl.app.main;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import hirondelle.date4j.DateTime;
import us.peripl.app.Constants;
import us.peripl.app.R;
import us.peripl.app.models.OpeningHours;
import us.peripl.app.models.Organisation;
import us.peripl.app.util.ParseLocalObject;

public class ActivityOpeningHours extends FragmentActivity {

    private final static String TAG = ActivityOpeningHours.class.getCanonicalName();

    private ParseLocalObject organisation;
    private boolean isPinnedOnly;
    CaldroidFragment caldroidFragment;
    TextView status;
    TextView hours;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_opening_hours);

        organisation = (ParseLocalObject) getIntent().getSerializableExtra(ParseLocalObject.PARSE_LOCAL_OBJECT);
        ((TextView) findViewById(R.id.name)).setText(organisation.getString(Organisation.NAME).toUpperCase());

        Organisation organisationShell = ParseObject.createWithoutData(Organisation.class, organisation.getObjectId());
        isPinnedOnly = getIntent().getBooleanExtra(Constants.IS_PINNED_ONLY, false);

        status = (TextView)findViewById(R.id.status);
        hours = (TextView)findViewById(R.id.hours);

        ParseQuery<OpeningHours> query = new ParseQuery<OpeningHours>(OpeningHours.class);
        if (isPinnedOnly) query.fromLocalDatastore();
        query.whereEqualTo(OpeningHours.OPENING_HOURS_OF, organisationShell);
        query.findInBackground(new FindCallback<OpeningHours>() {
            @Override
            public void done(final List<OpeningHours> openingHours, ParseException e) {

                if (e == null) {
                    //Query success
                    showCaldroid();

                    //Setup disabled dates
                    setDisabledDates(openingHours);

                    //Setup caldroid listener;
                    setCaldroidListeners(openingHours);

                    caldroidFragment.getCaldroidListener().onSelectDate(new Date(), caldroidFragment.getView());
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setCaldroidListeners(final List<OpeningHours> openingHours) {

        caldroidFragment.setCaldroidListener(new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {
                DateTime selectedDate = DateTime.forInstant(date.getTime(), TimeZone.getDefault());

                //Display selected date on calendar
                caldroidFragment.setSelectedDates(date, date);
                caldroidFragment.refreshView();

                //check if special case
                boolean isToday = false;
                boolean isTomorrow = false;

                DateTime today = DateTime.today(TimeZone.getDefault());
                if(today.numDaysFrom(selectedDate) == 0) isToday = true;
                else if (today.numDaysFrom(selectedDate) == 1) isTomorrow = true;

                boolean isSpecial = false;
                for(OpeningHours openingHoursEntry : openingHours) {
                    DateTime compareDate = DateTime.forInstant(openingHoursEntry.getOpens().getTime(), TimeZone.getDefault());
                    if(selectedDate.numDaysFrom(compareDate) == 0) {
                        //this is either openSpecial or closedSpecial
                        isSpecial = true;
                        if(openingHoursEntry.getDescription().equals("closedSpecial")) {

                            String statusString;
                            if(isToday) statusString = "Closed Today";
                            else if(isTomorrow) statusString = "Closed Tomorrow";
                            else statusString = "Closed on " + selectedDate.format("WWWW, D MMMM", Locale.ENGLISH);

                            status.setText(statusString);
                            hours.setText("For " + openingHoursEntry.getDayOfWeek());

                            break;

                        } else if (openingHoursEntry.getDescription().equals("openSpecial")) {

                            String statusString;
                            if(isToday) statusString = "Open Today";
                            else if(isTomorrow) statusString = "Open Tomorrow";
                            else statusString = "Open on " + selectedDate.format("WWWW, D MMMM", Locale.ENGLISH);

                            status.setText(statusString);

                            DateTime opening = DateTime.forInstant(openingHoursEntry.getOpens().getTime(), TimeZone.getDefault());
                            DateTime closes = DateTime.forInstant(openingHoursEntry.getCloses().getTime(), TimeZone.getDefault());

                            String openingTime = opening.format("h12:mma", Locale.ENGLISH).toLowerCase();
                            String closingTime = closes.format("h12:mma", Locale.ENGLISH).toLowerCase();

                            hours.setText(openingTime + " - " + closingTime);

                            break;
                        }
                    }
                }

                if(!isSpecial) {
                    for(OpeningHours openingHoursEntry : openingHours) {
                        if (selectedDate.format("WWWW", Locale.ENGLISH).equals(openingHoursEntry.getDayOfWeek())) {
                            //this is either openWeekly or closedWeekly
                            if(openingHoursEntry.getDescription().equals("closedWeekly")) {

                                String statusString;
                                if(isToday) statusString = "Closed Today";
                                else if(isTomorrow) statusString = "Closed Tomorrow";
                                else statusString = "Closed on " + selectedDate.format("WWWW", Locale.ENGLISH) + "s";

                                status.setText(statusString);
                                hours.setText("Weekly");

                                break;

                            } else if (openingHoursEntry.getDescription().equals("openWeekly")) {

                                String statusString;
                                if(isToday) statusString = "Open Today";
                                else if(isTomorrow) statusString = "Open Tomorrow";
                                else statusString = "Open on " + selectedDate.format("WWWW", Locale.ENGLISH) + "s";

                                status.setText(statusString);

                                DateTime opening = DateTime.forInstant(openingHoursEntry.getOpens().getTime(), TimeZone.getDefault());
                                DateTime closes = DateTime.forInstant(openingHoursEntry.getCloses().getTime(), TimeZone.getDefault());

                                String openingTime = opening.format("h12:mma", Locale.ENGLISH).toLowerCase();
                                String closingTime = closes.format("h12:mma", Locale.ENGLISH).toLowerCase();

                                hours.setText(openingTime + " - " + closingTime);
                                break;
                            }
                        }
                    }
                }

            }

            @Override
            public void onCaldroidViewCreated() {
                Date today = Calendar.getInstance().getTime();

                caldroidFragment.setSelectedDates(today, today);
                caldroidFragment.refreshView();
            }
        });
    }

    private void setDisabledDates(List<OpeningHours> openingHours) {
        ArrayList<Date> disabledDates = new ArrayList<Date>();

        for(OpeningHours openingHoursEntry: openingHours) {
            if(openingHoursEntry.getDescription().equals("closedSpecial")) {
                disabledDates.add(openingHoursEntry.getOpens());
            } else if(openingHoursEntry.getDescription().equals("closedWeekly")) {
                //TODO: Generate weekly date and add to disabled dates
            }
        }

        caldroidFragment.setDisableDates(disabledDates);
    }

    private void showCaldroid() {
        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_CLICK_ON_DISABLED_DATES, true);
        caldroidFragment.setArguments(args);
        getSupportFragmentManager().beginTransaction().add(R.id.container, caldroidFragment).commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_opening_hours, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
