package us.peripl.app.main;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import us.peripl.app.Constants;
import us.peripl.app.R;
import us.peripl.app.models.Organisation;
import us.peripl.app.util.ParseLocalObject;

public class FragmentInfo extends Fragment {

    private final static String TAG = FragmentInfo.class.getCanonicalName();

    private boolean isPinnedOnly;
    private ParseLocalObject organisation;

    public FragmentInfo() {
        // Required empty public constructor
    }

    public static FragmentInfo newInstance(ParseLocalObject organisation, boolean isPinnedOnly) {
        FragmentInfo fragment = new FragmentInfo();
        Bundle args = new Bundle();
        args.putSerializable(ParseLocalObject.PARSE_LOCAL_OBJECT, organisation);
        args.putBoolean(Constants.IS_PINNED_ONLY, isPinnedOnly);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            organisation = (ParseLocalObject) getArguments().getSerializable(ParseLocalObject.PARSE_LOCAL_OBJECT);
            isPinnedOnly = getArguments().getBoolean(Constants.IS_PINNED_ONLY, false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        TextView name = (TextView)view.findViewById(R.id.name);
        ImageView photo = (ImageView)view.findViewById(R.id.photo);
        TextView description = (TextView)view.findViewById(R.id.description);

        name.setText(organisation.getString(Organisation.NAME).toUpperCase());
        ImageLoader.getInstance().displayImage(organisation.getParseFileUrl(Organisation.PHOTO), photo);
        description.setText(organisation.getString(Organisation.DESCRIPTION));

        return view;
    }


}
