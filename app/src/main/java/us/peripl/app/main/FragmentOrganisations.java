package us.peripl.app.main;

import android.app.Activity;
import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.f2prateek.progressbutton.ProgressButton;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.List;

import us.peripl.app.Constants;
import us.peripl.app.R;
import us.peripl.app.adapters.ParseOrganisationAdapter;
import us.peripl.app.models.Organisation;
import us.peripl.app.tasks.ServicePinParseOrganisation;
import us.peripl.app.util.ParseLocalObject;

public class FragmentOrganisations extends ListFragment {

    private static final String TAG = FragmentOrganisations.class.getCanonicalName();

    private TextView emptyTextView;

    private int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;
    private BroadcastReceiver mReceiver;

    public FragmentOrganisations() {
    }

    public static FragmentOrganisations newInstance(int sectionNumber, boolean isPinnedOnly) {

        FragmentOrganisations fragment = new FragmentOrganisations();

        Bundle bundle = new Bundle();
        bundle.putInt(Constants.ARG_SECTION_NUMBER, sectionNumber);
        bundle.putBoolean(Constants.IS_PINNED_ONLY, isPinnedOnly);

        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((ActivityMain) activity).onSectionAttached(getArguments().getInt(Constants.ARG_SECTION_NUMBER));
        mReceiver = initReceiver();
        registerReceivers(activity);
    }

    private BroadcastReceiver initReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {

                    int position = intent.getIntExtra(ServicePinParseOrganisation.PIN_SERVICE_RECEIVER_INT_EXTRA_POSITION, -1);
                    boolean pin = intent.getBooleanExtra(ServicePinParseOrganisation.PIN_SERVICE_CMD, false);

                    if (intent.getAction().equals(ServicePinParseOrganisation.ACTION_PIN_SERVICE)) {
                        switch (intent.getIntExtra(ServicePinParseOrganisation.PIN_SERVICE_RECEIVER_CMD, -1)) {
                            case ServicePinParseOrganisation.ON_START:
                                Log.d(TAG, "Pinning/unpinning started for item: " + position);
                                setProgressButtonEnabled(position, false);
                                break;
                            case ServicePinParseOrganisation.ON_PROGRESS_UPDATE:
                                int progress = intent.getIntExtra(ServicePinParseOrganisation.PIN_SERVICE_RECEIVER_INT_EXTRA_PROGRESS, -1);
                                Log.d(TAG, "Pinning/unpinning progress for item: " + position + ", " + progress);
                                updateProgressButton(position, progress);
                                break;
                            case ServicePinParseOrganisation.ON_END:
                                Log.d(TAG, "Pinning/unpinning ended for item: " + position);
                                if (getArguments().getBoolean(Constants.IS_PINNED_ONLY))
                                    updateListView(pin);
                                setProgressButtonEnabled(position, true);
                                break;
                            case ServicePinParseOrganisation.ON_ERROR:
                                String error = intent.getStringExtra(ServicePinParseOrganisation.PIN_SERVICE_RECEIVER_STRING_EXTRA_ERROR);
                                Log.d(TAG, "Pinning/unpinning error for item: " + position + ", " + error);
                                break;
                            default:
                                Log.d(TAG, "Unknown PIN_SERVICE_RECEIVER_CMD");
                                break;
                        }
                    } else {
                        Log.d(TAG, "Unknown intent");
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private void updateListView(boolean pin) {
        if (!pin) ((ParseOrganisationAdapter) getListView().getAdapter()).loadObjects();
    }

    private void setProgressButtonEnabled(int position, boolean enabled) {
        int listViewPosition = position - 1;
        if (isViewVisible(listViewPosition)) {
            View view = getListView().getChildAt(listViewPosition);
            ProgressButton progressButton = (ProgressButton) view.findViewById(R.id.pin);
            progressButton.setEnabled(enabled);
        }
    }

    private void updateProgressButton(int position, int progress) {
        if (mScrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
            int listViewPosition = position - 1;
            if (isViewVisible(listViewPosition)) {
                View view = getListView().getChildAt(listViewPosition);
                ProgressButton progressButton = (ProgressButton) view.findViewById(R.id.pin);
                progressButton.setProgress(progress);
            }
        }
    }

    private boolean isViewVisible(int listViewPosition) {
        return (listViewPosition >= getListView().getFirstVisiblePosition() && listViewPosition <= getListView().getLastVisiblePosition());
    }

    private void registerReceivers(Context context) {
        IntentFilter mIntentFilter = new IntentFilter(ServicePinParseOrganisation.ACTION_PIN_SERVICE);
        LocalBroadcastManager.getInstance(context).registerReceiver(mReceiver, mIntentFilter);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_organisation, container, false);

        emptyTextView = (TextView) view.findViewById(android.R.id.empty);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        boolean isPinnedOnly = getArguments().getBoolean(Constants.IS_PINNED_ONLY);

        ParseOrganisationAdapter adapter = initAdapter(getActivity(), isPinnedOnly, Organisation.NAME);
        getListView().setAdapter(adapter);
        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                mScrollState = scrollState;
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

    }

    private ParseOrganisationAdapter initAdapter(final Context context, final boolean isPinnedOnly, final String sortOrder) {
        ParseOrganisationAdapter adapter = new ParseOrganisationAdapter(context, new ParseQueryAdapter.QueryFactory<Organisation>() {
            @Override
            public ParseQuery<Organisation> create() {
                ParseQuery<Organisation> query = new ParseQuery<Organisation>(Organisation.class);
                if (isPinnedOnly) query.fromLocalDatastore();
                query.orderByAscending(sortOrder);

                return query;
            }
        });

        adapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<Organisation>() {
            @Override
            public void onLoading() {
                ((Activity) context).setProgressBarIndeterminateVisibility(true);
                emptyTextView.setText("Periplus is updating this list...");
            }

            @Override
            public void onLoaded(List<Organisation> organisations, Exception e) {
                ((Activity) context).setProgressBarIndeterminateVisibility(false);
                if (e == null) {
                    Toast.makeText(context, "Completed updating list.", Toast.LENGTH_LONG).show();
                } else {
                    emptyTextView.setText("This list could not be updated.");
                }

            }
        });

        adapter.setImageKey(Organisation.PHOTO);
        adapter.setTextKey(Organisation.NAME);

        return adapter;
    }

    @Override
    public void onDetach() {
        unregisterReceivers(getActivity());
        mReceiver = null;
        super.onDetach();

    }

    private void unregisterReceivers(Context context) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mReceiver);
    }

    @Override
    public void onListItemClick(ListView listview, View v, int position, long id) {
        super.onListItemClick(listview, v, position, id);

        Organisation organisation = (Organisation) listview.getAdapter().getItem(position);

        final Intent intent = new Intent(getActivity(), ActivityOrganisation.class);
        intent.putExtra(ParseLocalObject.PARSE_LOCAL_OBJECT, new ParseLocalObject(organisation));

        organisation.isPinned(new Organisation.OnCheckPinnedState() {
            @Override
            public void checkPinnedState(boolean isPinned) {
                if (isPinned) {
                    intent.putExtra(Constants.IS_PINNED_ONLY, true);
                } else {
                    intent.putExtra(Constants.IS_PINNED_ONLY, false);
                }
                startActivity(intent);
            }
        });
    }

}
