package us.peripl.app.main;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import us.peripl.app.Constants;
import us.peripl.app.R;
import us.peripl.app.drawer.NavigationDrawerFragment;
import us.peripl.app.models.Exhibit;
import us.peripl.app.models.Organisation;
import us.peripl.app.util.ParseLocalObject;
import us.peripl.app.util.PeriLinkedHashMap;
import us.peripl.app.util.TypefaceSpan;

public class ActivityOrganisation extends Activity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final String TAG = ActivityOrganisation.class.getCanonicalName();
    private static final PeriLinkedHashMap<String, Integer> navItems = new PeriLinkedHashMap<String, Integer>();
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private ParseLocalObject organisation;
    private int prevSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        //setup roboto font in actionbar

        SpannableString s = new SpannableString(getResources().getString(R.string.app_name));
        s.setSpan(new TypefaceSpan(this, "Roboto-Thin.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getActionBar().setTitle(s);

        setContentView(R.layout.activity_organisation);

        //Retrieve bundle from parent activity
        organisation = (ParseLocalObject) getIntent().getSerializableExtra(ParseLocalObject.PARSE_LOCAL_OBJECT);

        //Setup nav drawer
        navItems.put("Collection", R.drawable.peri_stack_of_photos_selector);
        navItems.put("Admission", R.drawable.peri_ticket_selector);
        navItems.put("Address", R.drawable.peri_map_marker_selector);
        navItems.put("Opening Hours", R.drawable.peri_overtime_selector);
        navItems.put("Info", R.drawable.peri_info_selector);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = organisation.getString(Organisation.NAME);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    protected void onResume() {
        super.onResume();

        mNavigationDrawerFragment.checkItem(prevSelection);
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {

        FragmentManager fragmentManager = getFragmentManager();
        boolean isPinnedOnly = getIntent().getBooleanExtra(Constants.IS_PINNED_ONLY, false);

        Intent intent;

        Log.d(TAG, "isPinnedOnly: " + isPinnedOnly);

        switch (position) {
            case 0:
                fragmentManager.beginTransaction().replace(R.id.container, FragmentCollection.newInstance(position, organisation.getObjectId(), Exhibit.CANONICAL_NAME, isPinnedOnly)).commit();
                prevSelection = 0;
                break;
            case 1:
                intent = new Intent(ActivityOrganisation.this, ActivityDialog.class);
                intent.putExtra(ActivityDialog.DIALOG_TYPE, ActivityDialog.DialogType.ADMISSION);
                intent.putExtra(Constants.IS_PINNED_ONLY, isPinnedOnly);
                intent.putExtra(ParseLocalObject.PARSE_LOCAL_OBJECT, organisation);
                startActivity(intent);
                break;
            case 2:
                intent = new Intent(ActivityOrganisation.this, ActivityDialog.class);
                intent.putExtra(ActivityDialog.DIALOG_TYPE, ActivityDialog.DialogType.ADDRESS);
                intent.putExtra(Constants.IS_PINNED_ONLY, isPinnedOnly);
                intent.putExtra(ParseLocalObject.PARSE_LOCAL_OBJECT, organisation);
                startActivity(intent);
                break;
            case 3:
                intent = new Intent(ActivityOrganisation.this, ActivityOpeningHours.class);
                intent.putExtra(ActivityDialog.DIALOG_TYPE, ActivityDialog.DialogType.INFO);
                intent.putExtra(Constants.IS_PINNED_ONLY, isPinnedOnly);
                intent.putExtra(ParseLocalObject.PARSE_LOCAL_OBJECT, organisation);
                startActivity(intent);
                break;
            case 4:
                intent = new Intent(ActivityOrganisation.this, ActivityDialog.class);
                intent.putExtra(ActivityDialog.DIALOG_TYPE, ActivityDialog.DialogType.INFO);
                intent.putExtra(Constants.IS_PINNED_ONLY, isPinnedOnly);
                intent.putExtra(ParseLocalObject.PARSE_LOCAL_OBJECT, organisation);
                startActivity(intent);
                break;
        }

    }

    @Override
    public PeriLinkedHashMap<String, Integer> getNavItems() {
        return navItems;
    }

    @Override
    public int getInitialPosition() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        return 0;
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);

        SpannableString s = new SpannableString(mTitle);
        s.setSpan(new TypefaceSpan(this, "Roboto-Thin.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        actionBar.setTitle(s);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.organisation, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    public void onSectionAttached(int number) {

        mTitle = navItems.getKey(number);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_placeholder, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            //((ActivityOrganisation) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
