package us.peripl.app.main;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import us.peripl.app.R;
import us.peripl.app.drawer.NavigationDrawerFragment;
import us.peripl.app.util.PeriLinkedHashMap;
import us.peripl.app.util.TypefaceSpan;

public class ActivityMain extends Activity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final String TAG = ActivityMain.class.getCanonicalName();
    private static final PeriLinkedHashMap<String, Integer> navItems = new PeriLinkedHashMap<String, Integer>();
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private int prevSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        SpannableString s = new SpannableString(getResources().getString(R.string.app_name));
        s.setSpan(new TypefaceSpan(this, "Roboto-Thin.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getActionBar().setTitle(s);

        setContentView(R.layout.activity_main);

        navItems.put("Search", R.drawable.peri_search_selector);
        navItems.put("Pinned", R.drawable.peri_pin_selector);
        navItems.put("Museums", R.drawable.peri_museum_selector);
        navItems.put("Art Galleries", R.drawable.peri_gallery_selector);
        navItems.put("About Us", R.drawable.peri_about_selector);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    protected void onResume() {
        super.onResume();

        mNavigationDrawerFragment.checkItem(prevSelection);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments

        FragmentManager fragmentManager = getFragmentManager();
        FragmentOrganisations fragmentOrganisations;

        switch (position) {
            case 0: //Search
                Log.d(TAG, "NavItem: Search");
                fragmentManager.beginTransaction().replace(R.id.container, PlaceholderFragment.newInstance(position)).commit();
                prevSelection = 0;
                break;
            case 1: //Pinned
                fragmentOrganisations = (FragmentOrganisations) fragmentManager.findFragmentByTag(String.valueOf(position));
                if (fragmentOrganisations == null)
                    fragmentOrganisations = FragmentOrganisations.newInstance(position, true);
                fragmentManager.beginTransaction().replace(R.id.container, fragmentOrganisations, String.valueOf(position)).commit();
                prevSelection = 1;
                break;
            case 2: //Museums
                fragmentOrganisations = (FragmentOrganisations) fragmentManager.findFragmentByTag(String.valueOf(position));
                if (fragmentOrganisations == null)
                    fragmentOrganisations = FragmentOrganisations.newInstance(position, false);
                fragmentManager.beginTransaction().replace(R.id.container, fragmentOrganisations, String.valueOf(position)).commit();
                prevSelection = 2;
                break;
            case 3: //Art Galleries
                Log.d(TAG, "NavItem: Art Galleries");
                fragmentManager.beginTransaction().replace(R.id.container, PlaceholderFragment.newInstance(position)).commit();
                prevSelection = 3;
                break;
            case 4: //About Us
                Log.d(TAG, "NavItem: Abuts Us");
                Intent intent = new Intent(ActivityMain.this, ActivityAbout.class);
                startActivity(intent);
                //fragmentManager.beginTransaction().replace(R.id.container, PlaceholderFragment.newInstance(position)).commit();
                break;
        }
    }

    @Override
    public PeriLinkedHashMap getNavItems() {
        return navItems;
    }

    @Override
    public int getInitialPosition() {
        return 2;
    }

    @Override
    public int getCurrentPosition() {
        return -1;
    }

    public void onSectionAttached(int number) {

        mTitle = navItems.getKey(number);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);

        SpannableString s = new SpannableString(mTitle);
        s.setSpan(new TypefaceSpan(this, "Roboto-Thin.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        actionBar.setTitle(s);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_placeholder, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((ActivityMain) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
