package us.peripl.app;

import android.app.Application;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.parse.Parse;
import com.parse.ParseObject;

import us.peripl.app.models.Address;
import us.peripl.app.models.AdmissionCharge;
import us.peripl.app.models.Exhibit;
import us.peripl.app.models.OpeningHours;
import us.peripl.app.models.Organisation;

//First commit to Bitbucket!
public class ApplicationPeriplus extends Application {

    private static final String PARSE_APPLICATION_ID = "jOZrWHtrW2u8sCu71GEMI9GCCiYZruBfD7ChCzbI";
    private static final String PARSE_CLIENT_ID = "LnRoZOA4pbLvHRqY8rBoolVE2TTFuI1r8e46Uxaj";

    @Override
    public void onCreate() {
        Parse.enableLocalDatastore(this);
        ParseObject.registerSubclass(AdmissionCharge.class);
        ParseObject.registerSubclass(Exhibit.class);
        ParseObject.registerSubclass(Organisation.class);
        ParseObject.registerSubclass(Address.class);
        ParseObject.registerSubclass(OpeningHours.class);
        Parse.initialize(this, PARSE_APPLICATION_ID, PARSE_CLIENT_ID);

        DisplayImageOptions defaultDisplayImageOptions = new DisplayImageOptions.Builder()
                /*.showImageOnLoading(...) // resource or drawable */
                /*.showImageForEmptyUri(R.drawable.ic_about_peri_light) */ // resource or drawable
                /*.showImageOnFail(R.drawable.ic_error) // resource or drawable */
                .resetViewBeforeLoading(true)
                /*.delayBeforeLoading(1000)*/
                .cacheInMemory(true)
                .cacheOnDisk(true)
                /*.preProcessor(...)*/
                /*.postProcessor(...)*/
                /*.extraForDownloader(...)*/
                /*.considerExifParams(false)*/ // default
                /*.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)*/ // default
                /*.bitmapConfig(Bitmap.Config.ARGB_8888)*/ // default
                /*.decodingOptions(...)*/
                .displayer(new FadeInBitmapDisplayer(750, true, true, true)) // default
                /*.handler(new Handler())*/ // default
                .build();


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                /*.memoryCacheExtraOptions(480, 800)*/ // default = device screen dimensions
                /*.diskCacheExtraOptions(480, 800, CompressFormat.JPEG, 75, null)*/
                /*.taskExecutor(...)*/
                /*.taskExecutorForCachedImages(...)*/
                /*.threadPoolSize(3)*/ // default
                /*.threadPriority(Thread.NORM_PRIORITY - 1)*/ // default
                /*.tasksProcessingOrder(QueueProcessingType.FIFO)*/ // default
                /* .denyCacheImageMultipleSizesInMemory() */
                /*.memoryCache(new LruMemoryCache(2 * 1024 * 1024))*/
                /*.memoryCacheSize(2 * 1024 * 1024)*/
                /*.memoryCacheSizePercentage(13)*/ // default
                /*.diskCache(new UnlimitedDiscCache(cacheDir))*/ // default
                /*.diskCacheSize(50 * 1024 * 1024)*/
                /*.diskCacheFileCount(25)*/
                /*.diskCacheFileNameGenerator(new HashCodeFileNameGenerator())*/ // default
                /*.imageDownloader(new BaseImageDownloader(context))*/ // default
                /*.imageDecoder(new BaseImageDecoder())*/ // default
                .defaultDisplayImageOptions(defaultDisplayImageOptions) // default
                /*.writeDebugLogs()*/
                .build();

        ImageLoader.getInstance().init(config);

    }
}
