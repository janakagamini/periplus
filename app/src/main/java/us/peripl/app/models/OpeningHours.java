package us.peripl.app.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;

@ParseClassName("OpeningHours")
public class OpeningHours extends ParseObject {
    public final static String CLOSES = "closes";
    public final static String DAY_OF_WEEK = "dayOfWeek";
    public final static String OPENS = "opens";
    public final static String OPENING_HOURS_OF = "openingHoursOf";
    public final static String DESCRIPTION = "description";

    public Date getCloses() {
        return getDate(CLOSES);
    }

    public String getDayOfWeek() {
        return getString(DAY_OF_WEEK);
    }

    public Date getOpens() {
        return getDate(OPENS);
    }

    public String getOpeningHoursOf() {
        return getString(OPENING_HOURS_OF);
    }

    public String getDescription() {
        return getString(DESCRIPTION);
    }
}
