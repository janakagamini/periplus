package us.peripl.app.models;

import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

@ParseClassName("Organisations")
public class Organisation extends ParseObject {

    public final static String DESCRIPTION = "description";
    public final static String GEO = "geo";
    public final static String NAME = "name";
    public final static String PHOTO = "photo";
    public final static String SHORT_DESCRIPTION = "shortDescription";
    public final static String EXHIBIT_COUNT = "exhibitCount";

    private final static String TAG = Organisation.class.getName();

    public Organisation() {
    }

    public String getDescription() {
        return getString(DESCRIPTION);
    }

    public ParseGeoPoint getGeo() {
        return getParseGeoPoint(GEO);
    }

    public String getName() {
        return getString(NAME);
    }

    public ParseFile getPhoto() {
        return getParseFile(PHOTO);
    }

    public String getShortDescription() {
        return getString(SHORT_DESCRIPTION);
    }

    public int getExhibitcount() {
        return getInt(EXHIBIT_COUNT);
    }

    public void isPinned(final OnCheckPinnedState onCheckPinnedState) {
        ParseQuery<Organisation> query = new ParseQuery<Organisation>(Organisation.class);
        query.fromLocalDatastore();
        query.getInBackground(this.getObjectId(), new GetCallback<Organisation>() {
            @Override
            public void done(Organisation organisation, ParseException e) {
                if (organisation == null || e != null) {
                    onCheckPinnedState.checkPinnedState(false);
                } else {
                    onCheckPinnedState.checkPinnedState(true);
                }
            }
        });
    }

    public interface OnCheckPinnedState {
        public void checkPinnedState(boolean isPinned);
    }
}
