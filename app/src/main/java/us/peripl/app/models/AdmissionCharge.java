package us.peripl.app.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("AdmissionCharges")
public class AdmissionCharge extends ParseObject {

    public static final String DESCRIPTION = "description";
    public static final String PRICE_CURRENCY = "priceCurrency";
    public static final String PRICE = "price";
    public static final String CHARGE_FOR = "chargeFor";

    public String getDescription() {
        return getString(DESCRIPTION);
    }

    public String getPriceCurrency() {
        return getString(PRICE_CURRENCY);
    }

    public double getPrice() {
        return getDouble(PRICE);
    }

    public String getChargeFor() {
        return getString(CHARGE_FOR);
    }
}
