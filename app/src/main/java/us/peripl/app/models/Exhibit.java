package us.peripl.app.models;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

@ParseClassName("Exhibits")
public class Exhibit extends ParseObject {

    public static final String ARTIST_NAME = "artistName";
    public static final String CANONICAL_ARTIST_NAME = "canonicalArtistName";
    public static final String CANONICAL_NAME = "canonicalName";
    public static final String CANONICAL_REF = "canonicalRef";
    public static final String COPYRIGHT = "copyright";
    public static final String DESCRIPTION = "description";
    public static final String EXHIBIT_AT = "exhibitAt";
    public static final String IMAGE = "image";
    public static final String MEDIUM = "medium";
    public static final String NAME = "name";
    public static final String PLACE_OF_ORIGIN = "placeOfOrigin";
    public static final String REF = "REF";
    public static final String SIZE = "size";
    public static final String YEAR = "year";

    public Exhibit() {

    }

    public String getArtistName() {
        return getString(ARTIST_NAME);
    }

    public String getCanonicalArtistName() {
        return getString(CANONICAL_ARTIST_NAME);
    }

    public String getCanonicalName() {
        return getString(CANONICAL_NAME);
    }

    public String getCanonicalRef() {
        return getString(CANONICAL_REF);
    }

    public String getCopyright() {
        return getString(COPYRIGHT);
    }

    public String getDescription() {
        return getString(DESCRIPTION);
    }

    public ParseFile getImage() {
        return getParseFile(IMAGE);
    }

    public String getMedium() {
        return getString(MEDIUM);
    }

    public String getName() {
        return getString(NAME);
    }

    public String getPlaceOfOrigin() {
        return getString(PLACE_OF_ORIGIN);
    }

    public String getRef() {
        return getString(REF);
    }

    public String getSize() {
        return getString(SIZE);
    }

    public String getYear() {
        return getString(YEAR);
    }
}
