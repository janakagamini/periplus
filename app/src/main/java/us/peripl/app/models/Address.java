package us.peripl.app.models;


import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("AddressBook")
public class Address extends ParseObject {

    public final static String ADDRESS_COUNTRY = "addressCountry";
    public final static String ADDRESS_LOCALITY = "addressLocality";
    public final static String ADDRESS_REGION = "addressRegion";
    public final static String ADDRESS_OF = "addressOf";
    public final static String POSTAL_CODE = "postalCode";
    public final static String STREET_ADDRESS = "streetAddress";

    public Address() {
    }

    public String getAddressCountry() {
        return getString(ADDRESS_COUNTRY);
    }

    public String getAddressLocality() {
        return getString(ADDRESS_LOCALITY);
    }

    public String getAddressRegion() {
        return getString(ADDRESS_REGION);
    }

    public String getPostalCode() {
        return getString(POSTAL_CODE);
    }

    public String getStreetAddress() {
        return getString(STREET_ADDRESS);
    }

}
