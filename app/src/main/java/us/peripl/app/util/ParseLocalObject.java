package us.peripl.app.util;

import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

public class ParseLocalObject implements Serializable {

    public static final String PARSE_LOCAL_OBJECT = "parse_local_object";

    private final HashMap<String, Object> values = new HashMap<String, Object>();
    private final HashMap<String, String> fileUrls = new HashMap<String, String>();
    private final HashMap<String, double[]> geoPoints = new HashMap<String, double[]>();
    private String className;
    private Date createdAt;
    private String objectId;
    private Date updatedAt;

    public ParseLocalObject(ParseObject parseObject) {

        for (String key : parseObject.keySet()) {
            Class classType = parseObject.get(key).getClass();
            if (classType == Boolean.class ||
                    classType == byte[].class ||
                    classType == Date.class ||
                    classType == Double.class ||
                    classType == Integer.class ||
                    classType == Long.class ||
                    classType == Number.class ||
                    classType == String.class) {
                values.put(key, parseObject.get(key));
            } else if (classType == ParseFile.class) {
                fileUrls.put(key, ((ParseFile) parseObject.get(key)).getUrl());
            } else if (classType == ParseGeoPoint.class) {
                double[] latlong = {((ParseGeoPoint) parseObject.get(key)).getLatitude(), ((ParseGeoPoint) parseObject.get(key)).getLongitude()};
                geoPoints.put(key, latlong);
            }

            this.className = parseObject.getClassName();
            this.createdAt = parseObject.getCreatedAt();
            this.objectId = parseObject.getObjectId();
            this.updatedAt = parseObject.getUpdatedAt();
        }
    }

    public boolean getBoolean(String key) {
        if (values.containsKey(key)) {
            return (Boolean) values.get(key);
        } else {
            return false;
        }
    }

    public byte[] getBytes(String key) {
        if (values.containsKey(key)) {
            return (byte[]) values.get(key);
        } else {
            return null;
        }
    }

    public String getClassName() {
        return className;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getDate(String key) {
        if (values.containsKey(key)) {
            return (Date) values.get(key);
        } else {
            return null;
        }
    }

    public double getDouble(String key) {
        if (values.containsKey(key)) {
            return (Double) values.get(key);
        } else {
            return 0;
        }
    }

    public int getInt(String key) {
        if (values.containsKey(key)) {
            return (Integer) values.get(key);
        } else {
            return 0;
        }
    }

    public long getLong(String key) {
        if (values.containsKey(key)) {
            return (Long) values.get(key);
        } else {
            return 0;
        }
    }

    public Number getNumber(String key) {
        if (values.containsKey(key)) {
            return (Number) values.get(key);
        } else {
            return null;
        }
    }

    public String getObjectId() {
        return objectId;
    }

    public String getParseFileUrl(String key) {
        if (fileUrls.containsKey(key)) {
            return fileUrls.get(key);
        } else {
            return null;
        }
    }

    public double[] getParseGeoPointArray(String key) {
        if (geoPoints.containsKey(key)) {
            return geoPoints.get(key);
        } else {
            return null;
        }
    }

    public String getString(String key) {
        if (values.containsKey(key)) {
            return (String) values.get(key);
        } else {
            return null;
        }
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

}
