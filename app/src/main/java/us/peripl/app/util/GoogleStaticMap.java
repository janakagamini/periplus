package us.peripl.app.util;

import android.net.Uri;

public class GoogleStaticMap {

    //Sample URL
    //http://maps.googleapis.com/maps/api/staticmap?zoom=15&size=512x288&markers=color:red|38.888135,-77.02739&key=AIzaSyBoKuvzffjGAG9yF_5mUpcYvLbPsGHkDTE

    private static final String BASE_URL = "http://maps.googleapis.com/maps/api/staticmap";
    private static final String API_KEY = "AIzaSyBoKuvzffjGAG9yF_5mUpcYvLbPsGHkDTE";

    private static final String ZOOM = "zoom";
    private static final String SIZE = "size";
    private static final String SCALE = "scale";
    private static final String MARKERS = "markers";
    private static final String KEY = "key";

    private static String getZoomParamString(int zoom) {
        return String.valueOf(zoom);
    }

    private static String getSizeParamString(int width, int height) {
        return String.valueOf(width) + "x" + String.valueOf(height);
    }

    private static String getScaleParamString(int scale) {
        return String.valueOf(scale);
    }

    private static String getMarkerParamString(String color, double latitude, double longitude) {
        return "color:" + color + "|" + String.valueOf(latitude) + "," + String.valueOf(longitude);
    }

    public static String getStaticMapUrl(int zoom, int width, int height, int scale, String color, double latitude, double longitude) {

        Uri.Builder staticMapUri = Uri.parse(BASE_URL).buildUpon();

        staticMapUri.appendQueryParameter(ZOOM, getZoomParamString(zoom));
        staticMapUri.appendQueryParameter(SIZE, getSizeParamString(width, height));
        staticMapUri.appendQueryParameter(SCALE, getScaleParamString(scale));
        staticMapUri.appendQueryParameter(MARKERS, getMarkerParamString(color, latitude, longitude));
        staticMapUri.appendQueryParameter(KEY, API_KEY);

        return staticMapUri.build().toString();
    }

}
