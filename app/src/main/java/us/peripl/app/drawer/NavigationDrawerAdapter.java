package us.peripl.app.drawer;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.devspark.robototextview.widget.RobotoTextView;

import us.peripl.app.R;
import us.peripl.app.util.PeriLinkedHashMap;

public class NavigationDrawerAdapter extends BaseAdapter {

    final LayoutInflater inflater;
    final PeriLinkedHashMap<String, Integer> navItems;

    public NavigationDrawerAdapter(Context context, PeriLinkedHashMap<String, Integer> navItems) {

        inflater = ((Activity) context).getLayoutInflater();
        this.navItems = navItems;

    }

    @Override
    public int getCount() {
        return navItems.size();
    }

    @Override
    public Object getItem(int i) {
        return navItems.getKey(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_nav_drawer, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.text1 = (RobotoTextView) convertView.findViewById(R.id.text1);
            viewHolder.icon1 = (ImageView) convertView.findViewById(R.id.icon1);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.text1.setText(navItems.getKey(position));
        viewHolder.icon1.setImageResource(navItems.getValue(position));

        return convertView;
    }

    static class ViewHolder {
        RobotoTextView text1;
        ImageView icon1;
    }
}
