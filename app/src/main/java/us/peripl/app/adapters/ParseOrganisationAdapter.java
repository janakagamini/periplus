package us.peripl.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.f2prateek.progressbutton.ProgressButton;
import com.parse.ParseQueryAdapter;

import us.peripl.app.Constants;
import us.peripl.app.R;
import us.peripl.app.models.Organisation;
import us.peripl.app.tasks.ServicePinParseOrganisation;


public class ParseOrganisationAdapter extends ParseQueryAdapter<Organisation> {

    private static final String TAG = ParseOrganisationAdapter.class.getCanonicalName();
    private final LayoutInflater inflater;
    private Context context;

    public ParseOrganisationAdapter(Context context, QueryFactory<Organisation> queryFactory) {
        super(context, queryFactory);

        this.context = context;
        inflater = ((Activity) context).getLayoutInflater();
    }

    @Override
    public View getItemView(final Organisation organisation, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_peri_org, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.pin = (ProgressButton) convertView.findViewById(R.id.pin);
            viewHolder.shortDescription = (TextView) convertView.findViewById(R.id.shortDescription);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.pin.setVisibility(View.INVISIBLE);
        viewHolder.pin.setOnClickListener(null);

        organisation.isPinned(new Organisation.OnCheckPinnedState() {
            @Override
            public void checkPinnedState(boolean isPinned) {
                if (isPinned) {
                    viewHolder.pin.setPinned(true);
                    viewHolder.pin.setProgress(100);
                } else {
                    viewHolder.pin.setPinned(false);
                    viewHolder.pin.setProgress(0);
                }
                viewHolder.pin.setVisibility(View.VISIBLE);
            }
        });

        viewHolder.pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                organisation.isPinned(new Organisation.OnCheckPinnedState() {
                    @Override
                    public void checkPinnedState(boolean isPinned) {
                        if (isPinned) {
                            startService(organisation.getObjectId(), ((organisation.getExhibitcount() * 2) + 2), false);
                        } else {
                            startService(organisation.getObjectId(), ((organisation.getExhibitcount() * 2) + 2), true);
                        }
                    }
                });
            }
        });

        viewHolder.shortDescription.setText(organisation.getShortDescription());

        super.getItemView(organisation, convertView, parent);

        return convertView;
    }

    private void startService(String objectId, int maxProgress, boolean pin) {
        Intent serviceIntent = new Intent(context, ServicePinParseOrganisation.class);
        serviceIntent.putExtra(Constants.OBJECT_ID, objectId)
                .putExtra(ServicePinParseOrganisation.PIN_SERVICE_RECEIVER_INT_EXTRA_POSITION, 1)
                .putExtra(ServicePinParseOrganisation.PIN_SERVICE_MAX_PROGRESS, maxProgress)
                .putExtra(ServicePinParseOrganisation.PIN_SERVICE_CMD, pin);
        context.startService(serviceIntent);
    }

    static class ViewHolder {
        ProgressButton pin;
        TextView shortDescription;
    }
}