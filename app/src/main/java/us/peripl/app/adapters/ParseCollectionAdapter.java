package us.peripl.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.parse.ParseQueryAdapter;

import us.peripl.app.R;
import us.peripl.app.models.Exhibit;

public class ParseCollectionAdapter extends ParseQueryAdapter<Exhibit> {

    private final static String TAG = ParseCollectionAdapter.class.getCanonicalName();

    final LayoutInflater inflater;

    public ParseCollectionAdapter(Context context, QueryFactory<Exhibit> queryFactory) {
        super(context, queryFactory);

        inflater = ((Activity) context).getLayoutInflater();
    }

    @Override
    public View getItemView(Exhibit exhibit, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_peri_collection, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String imageUrl = "";

        try {
            imageUrl = exhibit.getParseFile(Exhibit.IMAGE).getUrl();
        } catch (NullPointerException e) {
            Log.d(TAG, exhibit.getName() + " has no image associated with it!");
        }

        ImageLoader.getInstance().displayImage(imageUrl, viewHolder.image, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                viewHolder.progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                viewHolder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                viewHolder.progressBar.setVisibility(View.GONE);
            }
        });

        super.getItemView(exhibit, convertView, parent);

        return convertView;
    }

    @Override
    public View getNextPageView(View view, ViewGroup parent) {

        if (view == null) {
            view = View.inflate(getContext(), R.layout.list_item_parse_next_page, null);
        }

        return view;
    }

    static class ViewHolder {
        ProgressBar progressBar;
        ImageView image;
    }
}
