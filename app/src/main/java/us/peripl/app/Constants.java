package us.peripl.app;

public class Constants {

    public static final String IS_PINNED_ONLY = "is_pinned_only";
    public static final String PARSE_SORT_KEY = "parse_sort_key";
    public static final String ARG_SECTION_NUMBER = "section_number";

    //Parse Object Ids
    public final static String OBJECT_ID = "objectId";
}
